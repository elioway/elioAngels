# Quickstart elioangels

- [elioangels Prerequisites](/elioangels/prerequisites.html)
- [Installing elioangels](/elioangels/installing.html)

## Nutshell

- Apps/modules used by two or more in of elio's groups.

## Creating a new elioangels app/module

1. Create a new app folder `your/repo/elioway/elioangels/my-angel`.
2. Get it working.
3. Document it using [elioangels/chisel](/elioangels/chisel).
4. Test it.
5. Brand it using [elioangels/generator-art](/elioangels/generator-art). Tell everyone about it.
6. Use it to do things **the elioWay**.
7. Push it to the <https://gitlab.com/elioangels/> .
