![](https://elioway.gitlab.io/elioangels/elio-Angels-logo.png)

> Where swift and winged help comes down from heaven, **the elioWay**

# elioangels ![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental")

## LAW

- Thous shalt have conceptial app names with sub folders named after the primary disambiguating platform name for different versions, written in different languages.
- There can be a package.json file in the main folder, but it should be formatted in **the elioWay**. Sub folders will hold the packageJson for that specific nodeapp (in React, FeatherJs, or pure "nodejs," etc)

Apps, libraries, modules or projects intended for elioWay development.

- [elioangels Documentation](https://elioway.gitlab.io/elioangels)

## Nutshell

- [elioangels Quickstart](https://elioway.gitlab.io/elioangels/quickstart.html)
- [elioangels Credits](https://elioway.gitlab.io/elioangels/credits.html)

![](https://elioway.gitlab.io/elioangels/apple-touch-icon.png)

## License

[MIT](LICENSE) [Tim Bushell](mailto:theElioWay@gmail.com)
